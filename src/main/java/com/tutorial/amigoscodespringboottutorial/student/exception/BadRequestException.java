package com.tutorial.amigoscodespringboottutorial.student.exception;

public class BadRequestException extends RuntimeException {


    public BadRequestException(String message) {
        super(message);
    }

}
