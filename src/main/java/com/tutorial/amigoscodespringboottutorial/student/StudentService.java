package com.tutorial.amigoscodespringboottutorial.student;

import com.tutorial.amigoscodespringboottutorial.student.exception.BadRequestException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Service
public class StudentService {
    private StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public void addStudent(Student student) {
        boolean existsEmail  = studentRepository.selectEmailExists(student.getEmail());
        if (existsEmail) {
            throw new BadRequestException("email taken");
        }
        studentRepository.save(student);
    }

    @Transactional
    public void deleteStudent(Long studentId) {
        boolean exist = studentRepository.existsById(studentId);
        if (!exist) {
            throw new IllegalStateException("student with id " + studentId + " does not exist");
        }
        studentRepository.deleteById(studentId);
    }

    @Transactional
    public void updateStudent(Long studentId, String name, String email) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new IllegalStateException("student with id " + studentId + " does not exist"));
        if (name != null && name.length() > 0 && !Objects.equals(student.getName(), name)) {
                    student.setName(name);
        }
        if (email != null && email.length() > 0 && !Objects.equals(student.getEmail(), email)) {
            boolean existsEmail = studentRepository.selectEmailExists(email);
            if (existsEmail) {
                throw new IllegalStateException("email taken");
            }
            student.setEmail(email);
        }
    }
}
