package com.tutorial.amigoscodespringboottutorial.student;

public enum Gender {
    MALE, FEMALE
}
