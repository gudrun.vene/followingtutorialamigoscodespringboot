package com.tutorial.amigoscodespringboottutorial.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
public class StudentConfig {

//    @Bean
//    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
//        return args -> {
//            Student bw = new Student(
//                    "BW",
//                    "blackwidow@gmail.com",
//                    LocalDate.of(1983, 1, 12)
//            );
//
//            Student fury = new Student(
//                    "Fury",
//                    "nickfuryw@gmail.com",
//                    LocalDate.of(1955, 4, 19)
//            );
//
//            studentRepository.saveAll(
//                    List.of(bw, fury)
//            );
//        };
//    }
}
