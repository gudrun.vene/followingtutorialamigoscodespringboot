package com.tutorial.amigoscodespringboottutorial.student;

import javax.persistence.*;


@Entity
@Table
public class Student {

    private Gender gender;
    @Id
    @SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_sequence"
    )
    private Long id;
    private String name;
    private String email;

//    private LocalDate dateOfBirth;
//    @Transient
//    private Integer age;

    public Student() {
    }

    public Student(String name, String email, Gender gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    //    public Student(Long id,
//                   String name,
//                   String email,
//                   LocalDate dateOfBirth) {
//        this.id = id;
//        this.name = name;
//        this.email = email;
//        this.dateOfBirth = dateOfBirth;
//    }

//    public Student(String name,
//                   String email,
//                   LocalDate dateOfBirth
//    ) {
//        this.name = name;
//        this.email = email;
//        this.dateOfBirth = dateOfBirth;
//    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public LocalDate getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(LocalDate dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
//
//    public Integer getAge() {
//        return Period.between(this.dateOfBirth, LocalDate.now()).getYears();
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }

    @Override
    public String toString() {
        return "Student{" +
                "gender=" + gender +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }


//    @Override
//    public String toString() {
//        return "Student{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", email='" + email + '\'' +
//                ", dateOfBirth=" + dateOfBirth +
//                ", age=" + age +
//                '}';
//    }
}
