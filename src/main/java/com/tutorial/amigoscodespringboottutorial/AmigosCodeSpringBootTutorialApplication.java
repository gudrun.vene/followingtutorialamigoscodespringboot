package com.tutorial.amigoscodespringboottutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmigosCodeSpringBootTutorialApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmigosCodeSpringBootTutorialApplication.class, args);

    }


}
