package com.tutorial.amigoscodespringboottutorial.student;

import com.tutorial.amigoscodespringboottutorial.student.exception.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

//@ExtendWith(MockitoExtension.class)
@SpringBootTest
class StudentServiceTest {
    @Mock
    private StudentRepository studentRepository;

    private StudentService underTest;

    @BeforeEach
    void setUp() {
        underTest = new StudentService(studentRepository);
    }


    Student student = new Student(
            "Shuri", "shuri@gmail.com", Gender.FEMALE
    );



    @Test
    void canGetAllStudents() {
        //when
        underTest.getAllStudents();
        // then
        verify(studentRepository).findAll();
    }

    @Test
    void canAddStudent() {
        // given
        Student student = new Student(
                "Shuri", "shuri@gmail.com", Gender.FEMALE
        );
        // when
        underTest.addStudent(student);
        // then
        ArgumentCaptor<Student> studentArgumentCaptor = ArgumentCaptor.forClass(Student.class);
        verify(studentRepository).save(studentArgumentCaptor.capture());
        Student capturedStudent = studentArgumentCaptor.getValue();
        // id=null
//        System.out.println(capturedStudent.toString());
        assertThat(capturedStudent).isEqualTo(student);
    }

    @Test
    void willThrowWhenEmailIsTaken() {
        // given
        Student student = new Student(
                "Shuri", "shuri@gmail.com", Gender.FEMALE
        );

        given(studentRepository.selectEmailExists(student.getEmail()))
                .willReturn(true);
        // when
        // then
        assertThatThrownBy(() -> underTest.addStudent(student))
                .isInstanceOf(BadRequestException.class)
                .hasMessageContaining("email taken");
        verify(studentRepository, never()).save(any());

    }

    @Test
    void canDeleteStudent() {


    }

    @Test
    void canNotDeleteStudent() {

    }

}