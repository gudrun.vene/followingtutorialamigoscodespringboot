package com.tutorial.amigoscodespringboottutorial.student;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldCheckIfStudentExistsByEmail() {
        // given
        String email = "shuri@gmail.com";
        Student student = new Student(
                "Shuri", email, Gender.FEMALE
        );
        underTest.save(student);
        // when
        boolean expected = underTest.selectEmailExists(email);
        // then
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldCheckIfStudentEmailDoesNotExist() {
        // given
        String email = "shuri@gmail.com";
        // when
        boolean expected = underTest.selectEmailExists(email);
        // then
        assertThat(expected).isFalse();
    }
}